package com.hsh.learning;


import javax.servlet.Filter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import com.hsh.learning.filter.SecondFilter;
import com.hsh.learning.listener.SecondRequestListener;
import com.hsh.learning.servlet.SecondServlet;

@SpringBootApplication
//可以扫描到first包下的servlet类，且需要使用@WebServlet或@WebFilter
@ServletComponentScan(value= {"com.hsh.learning.servlet","com.hsh.learning.filter","com.hsh.learning.listener"})
@ComponentScan("com.hsh.learning")
public class Application extends SpringBootServletInitializer{
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
       return application.sources(Application.class);
    }
	 
	@Bean
	public ServletRegistrationBean getServletRegistrationBean() {  //一定要返回ServletRegistrationBean
	  	//放入自己的Servlet对象实例
	 	ServletRegistrationBean bean = new ServletRegistrationBean(new SecondServlet());   
        bean.addUrlMappings("/secondServlet");  //访问路径值
        return bean;
	}
	
	
	@Bean
    public FilterRegistrationBean<Filter> myFilterBean(SecondFilter filter) {
        FilterRegistrationBean<Filter> filterRegistrationBean = new FilterRegistrationBean<>();
        filterRegistrationBean.setFilter(filter);//设置为自定义的过滤器SecondFilter
        filterRegistrationBean.addUrlPatterns("/*");//拦截所有请求
        return filterRegistrationBean;
    }
	
	@Bean
	public ServletListenerRegistrationBean<SecondRequestListener> getListener(){
		ServletListenerRegistrationBean<SecondRequestListener> bean = 
				new ServletListenerRegistrationBean<>(new SecondRequestListener());
		return bean;
	}
	
}

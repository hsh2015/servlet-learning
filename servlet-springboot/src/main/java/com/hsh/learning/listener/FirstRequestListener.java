package com.hsh.learning.listener;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class FirstRequestListener implements ServletRequestListener {

	@Override
	public void requestDestroyed(ServletRequestEvent sre) {
		System.out.println("FirstRequestListener-requestDestroyed()方法");
	}

	@Override
	public void requestInitialized(ServletRequestEvent sre) {
		System.out.println("FirstRequestListener-requestInitialized()方法");
	}

}

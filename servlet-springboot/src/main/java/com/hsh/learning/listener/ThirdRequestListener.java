package com.hsh.learning.listener;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;

public class ThirdRequestListener implements ServletRequestListener {

	@Override
	public void requestDestroyed(ServletRequestEvent sre) {
		System.out.println("ThirdRequestListener-requestDestroyed()方法");
	}

	@Override
	public void requestInitialized(ServletRequestEvent sre) {
		System.out.println("ThirdRequestListener-requestInitialized()方法");
	}

}

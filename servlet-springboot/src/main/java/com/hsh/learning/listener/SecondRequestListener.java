package com.hsh.learning.listener;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;

public class SecondRequestListener implements ServletRequestListener {

	@Override
	public void requestDestroyed(ServletRequestEvent sre) {
		System.out.println("SecondRequestListener-requestDestroyed()方法");
	}

	@Override
	public void requestInitialized(ServletRequestEvent sre) {
		System.out.println("SecondRequestListener-requestInitialized()方法");
	}

}

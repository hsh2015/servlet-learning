package com.hsh.learning.config;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.hsh.learning.servlet.ThirdServlet;

@Configuration
public class ServletConfig {
	@Bean
	public ServletRegistrationBean getServletRegistrationBean() {  //一定要返回ServletRegistrationBean
	  	//放入自己的Servlet对象实例
	 	ServletRegistrationBean bean = new ServletRegistrationBean(new ThirdServlet());   
        bean.addUrlMappings("/thirdServlet");  //访问路径值
        return bean;
	}
}

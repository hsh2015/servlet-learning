package com.hsh.learning.config;

import javax.servlet.Filter;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.hsh.learning.filter.ThirdFilter;
/**
 * 第一种方式：通过代码实现，使用了Servlet3.0的新特性，</br>
 * 在Spring中封装成了FilterRegistrationBean对象，就如下面的例子所示。</br>
 * 第二种方式：在web.xml文件中配置，如下所示：
 * <pre class="code">
 * {@code<filter>
 *	  <filter-name>myFilter</filter-name>
 *	  <filter-class>com.text.filter.myFilter</filter-class>
 * </filter>
 * <filter-mapping>
 *	  <filter-name>myFilter</filter-name>
 *	  <url-pattern>/*</url-pattern>
 * </filter-mapping>}
 * </pre>
 * @param filter
 * @return
 */
@Configuration
public class FilterConfig {
	
	@Bean
    public FilterRegistrationBean<Filter> myFilterBean2(ThirdFilter filter) {
        FilterRegistrationBean<Filter> filterRegistrationBean = new FilterRegistrationBean<>();
        filterRegistrationBean.setFilter(filter);//设置为自定义的过滤器FirstFilter
        filterRegistrationBean.addUrlPatterns("/*");//拦截所有请求
        return filterRegistrationBean;
    }
	
}

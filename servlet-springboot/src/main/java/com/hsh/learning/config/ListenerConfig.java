package com.hsh.learning.config;


import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.hsh.learning.listener.ThirdRequestListener;

@Configuration
public class ListenerConfig {
	
	@Bean
	public ServletListenerRegistrationBean<ThirdRequestListener> getListener2(){
		ServletListenerRegistrationBean<ThirdRequestListener> bean = 
				new ServletListenerRegistrationBean<>(new ThirdRequestListener());
		return bean;
	}
	
}

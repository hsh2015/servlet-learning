package com.hsh.listener.session.bean;

import javax.servlet.http.HttpSessionActivationListener;
import javax.servlet.http.HttpSessionEvent;


public class MyUserOfActivation extends MyUser implements HttpSessionActivationListener {

	private static final long serialVersionUID = 1L;

	@Override
	public void sessionWillPassivate(HttpSessionEvent se) {
		System.out.println("MyUserOfActivation-sessionWillPassivate()方法，JavaBean对象钝化……");
	}

	@Override
	public void sessionDidActivate(HttpSessionEvent se) {
		System.out.println("MyUserOfActivation-sessionDidActivate()方法，JavaBean对象激活……");
	}

}

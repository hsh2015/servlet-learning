package com.hsh.listener.session.bean;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

public class MyUserOfBinding extends MyUser implements HttpSessionBindingListener {

	private static final long serialVersionUID = 1L;

	@Override
	public void valueBound(HttpSessionBindingEvent event) {
		System.out.println("MyUserOfBinding-valueBound()方法，JavaBean对象绑定……");
	}

	@Override
	public void valueUnbound(HttpSessionBindingEvent event) {
		System.out.println("MyUserOfBinding-valueUnbound()方法，JavaBean对象解绑……");
	}

}

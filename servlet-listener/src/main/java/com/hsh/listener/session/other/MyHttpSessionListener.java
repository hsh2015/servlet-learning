package com.hsh.listener.session.other;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionIdListener;
import javax.servlet.http.HttpSessionListener;

@WebListener
public class MyHttpSessionListener implements HttpSessionListener, HttpSessionIdListener{

	@Override
	public void sessionCreated(HttpSessionEvent se) {
		System.out.println("MyHttpSessionListener-sessionCreated()方法，HttpSession初始化……");
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
		System.out.println("MyHttpSessionListener-sessionDestroyed()方法，HttpSession销毁……");
	}

	@Override
	public void sessionIdChanged(HttpSessionEvent se, String oldSessionId) {
		System.out.println("MyHttpSessionListener-sessionIdChanged()方法，HttpSessionID发生变化……");
	}

}

package com.hsh.listener.session.bean;

import java.io.Serializable;

public class MyUser implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String uername;
	private String password;
	private String note;

	public String getUername() {
		return uername;
	}

	public void setUername(String uername) {
		this.uername = uername;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}
}

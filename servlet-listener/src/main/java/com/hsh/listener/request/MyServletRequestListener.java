package com.hsh.listener.request;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class MyServletRequestListener implements ServletRequestListener {

	@Override
	public void requestDestroyed(ServletRequestEvent sre) {
		System.out.println("MyServletRequestListener-requestDestroyed()方法……");
	}

	@Override
	public void requestInitialized(ServletRequestEvent sre) {
		System.out.println("MyServletRequestListener-requestInitialized()方法……");
	}

}

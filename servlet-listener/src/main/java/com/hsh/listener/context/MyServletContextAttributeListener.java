package com.hsh.listener.context;

import javax.servlet.ServletContextAttributeEvent;
import javax.servlet.ServletContextAttributeListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class MyServletContextAttributeListener implements ServletContextAttributeListener {

	@Override
	public void attributeAdded(ServletContextAttributeEvent scae) {
		System.out.println("MyServletContextAttributeListener-attributeAdded()方法，ServletContext中属性被添加……");
	}

	@Override
	public void attributeRemoved(ServletContextAttributeEvent scae) {
		System.out.println("MyServletContextAttributeListener-attributeRemoved()方法，ServletContext中属性被移除……");
	}

	@Override
	public void attributeReplaced(ServletContextAttributeEvent scae) {
		System.out.println("MyServletContextAttributeListener-attributeReplaced()方法，ServletContext中属性被替换……");
	}

}

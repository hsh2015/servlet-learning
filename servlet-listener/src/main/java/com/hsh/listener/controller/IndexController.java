package com.hsh.listener.controller;

import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.hsh.listener.session.bean.MyUserOfActivation;
import com.hsh.listener.session.bean.MyUserOfBinding;

@Controller
public class IndexController {
	
	@RequestMapping("/index")
    public String session(HttpServletRequest request, HttpServletResponse response){
		HttpSession session = request.getSession();
		session.setAttribute("id", UUID.randomUUID().toString());
        return "SessionTest";
    }
	
	@RequestMapping("/logout")
    public String logout(HttpServletRequest request, HttpServletResponse response){
		HttpSession session = request.getSession();
        session.invalidate();
        System.out.println("Session对象被销毁了");
        return "SessionTest";
    }
	
	@RequestMapping("/testSessionAttribute")
    public String testSessionAttribute(HttpServletRequest request, HttpServletResponse response){
		HttpSession session = request.getSession();
		System.out.println("########## 添加  ##############");
		session.setAttribute("username", "张三");
		session.setAttribute("age", 15);
		System.out.println("########### 移除  #############");
		session.removeAttribute("username");
		System.out.println("########## 替换  ##############");
		session.setAttribute("age", 16);
        return "SessionTest";
    }
	
	@RequestMapping("/bindTest")
    public String bindTest(HttpServletRequest request, HttpServletResponse response){
        HttpSession session = request.getSession();
        MyUserOfBinding user = new MyUserOfBinding();
        user.setUername("user1");
        session.setAttribute("user", user);
        System.out.println("########################");
        session.removeAttribute("user");
		return "SessionTest";
    }
	
	@RequestMapping("/activationTest")
    public String activationTest(HttpServletRequest request, HttpServletResponse response){
        HttpSession session = request.getSession();
        MyUserOfActivation user = new MyUserOfActivation();
        user.setUername("user1");
        session.setAttribute("user", user);
        
		return "SessionTest";
    }
}

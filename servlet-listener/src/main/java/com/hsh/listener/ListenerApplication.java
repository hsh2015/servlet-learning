package com.hsh.listener;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;


//可以扫描到com.hsh.listener包下的监听器类
@ServletComponentScan(value = { "com.hsh.listener.session.bean" })
@ComponentScan
@SpringBootApplication
public class ListenerApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(ListenerApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(ListenerApplication.class);
	}
}

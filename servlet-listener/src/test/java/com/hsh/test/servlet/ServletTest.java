package com.hsh.test.servlet;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.hsh.listener.ListenerApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ListenerApplication.class)
//web请求容器
@WebAppConfiguration
public class ServletTest {
	@Autowired
    WebApplicationContext context;
	//模拟请求
    MockMvc mvc;
	@Before
    public void setUp() {
		 mvc=MockMvcBuilders.webAppContextSetup(context).build();
    }

    @Test
    public void add() {
    	
    }
}
